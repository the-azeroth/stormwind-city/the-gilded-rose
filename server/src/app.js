const express = require("express");
const app = express();
const port = 8080;

app.get("/health/ping", (req, res) => {
  setTimeout(() => res.send("Pong!"), 5000);
});

app.listen(port, () => console.log(`Server now listening on port ${port}`));
