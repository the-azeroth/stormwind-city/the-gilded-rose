import { join, resolve } from 'path';
import 'webpack-dev-server';
import { Configuration, HotModuleReplacementPlugin } from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

const config: Configuration = {
    mode: 'development',
    entry: join(__dirname, 'src', 'main.ts'),
    devtool: 'inline-source-map',
    plugins: [
        new HtmlWebpackPlugin({
            // hash: true
        }),
        // new MiniCssExtractPlugin(),
        // new HotModuleReplacementPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            // {
            //     test: /\.scss$/,
            //     use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            // }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    devServer: {
        static: {
            directory: resolve(__dirname, 'dist')
        },
        port: 4000,
        host: '0.0.0.0',
        compress: true
    },
    output: {
        filename: '[name].bundle.js',
        path: resolve(__dirname, 'dist'),
        clean: true
    },
    optimization: {
        runtimeChunk: 'single'
    }
}

export default config;