import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'app', loadChildren: () => import('@app/admin/admin.module').then(e => e.AdminModule) },
      { path: 'test', loadChildren: () => import('@app/test/test.module').then(e => e.TestModule) },
      {
        path: 'test-primeng',
        loadChildren: () => import('@app/test-primeng/test-primeng.module').then(e => e.TestPrimengModule)
      },
      {
        path: 'test-rxjs',
        loadChildren: () => import('@app/test-rxjs/test-rxjs.module').then(e => e.TestRxjsModule)
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      relativeLinkResolution: 'legacy',
      scrollPositionRestoration: 'top',
      onSameUrlNavigation: 'reload'
      // preloadingStrategy: 'PreloadAllModules'
    })
  ],
  exports: [RouterModule]
})
export class AppRouting {}
