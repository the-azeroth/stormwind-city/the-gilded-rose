import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardService } from '@common/components/dashboard/dashboard.service';
import { MenuModule } from 'primeng/menu';

@NgModule({
  declarations: [DashboardComponent],
  imports: [CommonModule, MenuModule],
  exports: [DashboardComponent],
  providers: [DashboardService]
})
export class DashboardModule {}
