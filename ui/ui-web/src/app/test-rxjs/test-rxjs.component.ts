import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, interval, Observable, Subscription, switchMap, timer } from 'rxjs';

@Component({
  selector: 'app-test-rxjs',
  templateUrl: './test-rxjs.component.html',
  styleUrls: ['./test-rxjs.component.scss']
})
export class TestRxjsComponent implements OnInit, OnDestroy {
  public status = 'None';

  private subscription: Subscription = new Subscription();

  constructor(private httpClient: HttpClient) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public onStart(): void {
    console.log('Start Polling Process');
    const polling = timer(0, 5500)
      .pipe(switchMap(() => this.healthCheck()))
      .subscribe(res => {
        this.status = res;
      });
    this.subscription.add(polling);
  }

  public onRefresh(): void {
    console.log('Refresh Polling Process');
    this.subscription.unsubscribe();
    const polling = timer(0, 5500)
      .pipe(switchMap(() => this.healthCheck()))
      .subscribe(res => {
        this.status = res;
      });
    this.subscription.add(polling);
  }

  private healthCheck(): Observable<string> {
    return this.httpClient.get<string>('/health/ping');
  }
}
