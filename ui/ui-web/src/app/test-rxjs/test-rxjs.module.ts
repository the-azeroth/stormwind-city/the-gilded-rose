import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestRxjsRouting } from './test-rxjs.routing';
import { TestRxjsComponent } from './test-rxjs.component';

@NgModule({
  declarations: [TestRxjsComponent],
  imports: [CommonModule, TestRxjsRouting]
})
export class TestRxjsModule {}
