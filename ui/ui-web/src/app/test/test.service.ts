import { TestEnum } from '@app/test/test';

export class TestService {
  constructor(private _type: TestEnum) {}

  get type(): TestEnum {
    return this._type;
  }
}
