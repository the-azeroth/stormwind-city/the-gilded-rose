import { Component, Inject, OnInit } from '@angular/core';
import { TestService } from '@app/test/test.service';
import { LCC_EXPORT, PROPOSAL_EXPORT } from '@app/test/test.module';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  constructor(
    @Inject(PROPOSAL_EXPORT) private proposalService: TestService,
    @Inject(LCC_EXPORT) private lccService: TestService
  ) {}

  ngOnInit(): void {
    console.log(this.proposalService.type);
    console.log(this.lccService.type);
  }
}
