import { InjectionToken, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestRouting } from './test.routing';
import { TestComponent } from './test.component';
import { TestService } from '@app/test/test.service';
import { TestEnum } from '@app/test/test';

export const PROPOSAL_EXPORT = new InjectionToken<TestService>('PROPOSAL_EXPORT');
export const LCC_EXPORT = new InjectionToken<TestService>('LCC_EXPORT');

@NgModule({
  declarations: [TestComponent],
  imports: [CommonModule, TestRouting],
  providers: [
    {
      provide: PROPOSAL_EXPORT,
      useFactory: () => new TestService(TestEnum.PROPOSAL)
    },
    {
      provide: LCC_EXPORT,
      useFactory: () => new TestService(TestEnum.LCC)
    }
  ]
})
export class TestModule {}
