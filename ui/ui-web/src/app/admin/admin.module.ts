import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRouting } from './admin.routing';
import { AdminComponent } from './admin.component';
import { DashboardModule } from '@common/components/dashboard/dashboard.module';

@NgModule({
  declarations: [AdminComponent],
  imports: [AdminRouting, CommonModule, DashboardModule]
})
export class AdminModule {}
