import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'advance-search-bar',
        loadChildren: () =>
          import('@app/test-primeng/test-advance-search-bar/test-advance-search-bar.module').then(
            e => e.TestAdvanceSearchBarModule
          )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestPrimengRouting {}
