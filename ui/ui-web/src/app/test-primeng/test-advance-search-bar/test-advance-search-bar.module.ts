import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestAdvanceSearchBarRouting } from './test-advance-search-bar.routing';
import { TestAdvanceSearchBarComponent } from './test-advance-search-bar.component';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { CalendarModule } from 'primeng/calendar';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { BreadcrumbModule } from 'primeng/breadcrumb';

@NgModule({
  declarations: [TestAdvanceSearchBarComponent],
  imports: [
    BreadcrumbModule,
    ButtonModule,
    CalendarModule,
    CommonModule,
    InputTextModule,
    OverlayPanelModule,
    RippleModule,
    TestAdvanceSearchBarRouting
  ]
})
export class TestAdvanceSearchBarModule {}
