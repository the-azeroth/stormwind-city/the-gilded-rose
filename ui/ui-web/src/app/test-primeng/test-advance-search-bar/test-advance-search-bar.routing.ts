import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestAdvanceSearchBarComponent } from '@app/test-primeng/test-advance-search-bar/test-advance-search-bar.component';

const routes: Routes = [{ path: '', component: TestAdvanceSearchBarComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestAdvanceSearchBarRouting {}
