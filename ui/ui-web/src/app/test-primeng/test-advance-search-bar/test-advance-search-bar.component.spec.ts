import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestAdvanceSearchBarComponent } from './test-advance-search-bar.component';

describe('TestAdvanceSearchBarComponent', () => {
  let component: TestAdvanceSearchBarComponent;
  let fixture: ComponentFixture<TestAdvanceSearchBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestAdvanceSearchBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestAdvanceSearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
