import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-test-advance-search-bar',
  templateUrl: './test-advance-search-bar.component.html',
  styleUrls: ['./test-advance-search-bar.component.scss']
})
export class TestAdvanceSearchBarComponent implements OnInit {
  items: MenuItem[] = [{ label: 'Dashboard' }, { label: 'Test PrimeNG' }, { label: 'Advance Search Bar' }];

  constructor() {}

  ngOnInit(): void {}

  onSearch(): void {}
}
