import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test-primeng',
  template: ` <router-outlet></router-outlet> `,
  styles: []
})
export class TestPrimengComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
