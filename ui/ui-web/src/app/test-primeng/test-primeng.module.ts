import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestPrimengRouting } from './test-primeng.routing';
import { TestPrimengComponent } from './test-primeng.component';

@NgModule({
  declarations: [
    TestPrimengComponent
  ],
  imports: [CommonModule, TestPrimengRouting]
})
export class TestPrimengModule {}
