import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'workflow', loadChildren: () => import('./workflow/workflow.module').then(m => m.WorkflowModule) }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      relativeLinkResolution: 'legacy',
      scrollPositionRestoration: 'top',
      onSameUrlNavigation: 'reload'
      // preloadingStrategy: 'PreloadAllModules'
    })
  ],
  exports: [RouterModule]
})
export class AppRouting {}
