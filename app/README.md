# APP

Angular CLI version   13.1.4

```shell
ng new app --create-application=false --new-project-root --package-manager=yarn --skip-install --strict=false --style=scss
cd app
ng add @angular-eslint/schematics

ng g application app-portal --style=scss --strict=false --routing=false
ng g application app-workflow --style=scss --strict=false --routing=false

ng add @angular-architects/module-federation
```

```shell
yarn add -D prettier eslint-config-prettier eslint-plugin-header eslint-plugin-import eslint-plugin-jsdoc eslint-plugin-rxjs-angular
```
