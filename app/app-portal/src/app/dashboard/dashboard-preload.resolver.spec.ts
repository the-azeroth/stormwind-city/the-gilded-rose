import { TestBed } from '@angular/core/testing';

import { DashboardPreloadResolver } from './dashboard-preload.resolver';

describe('DashboardPreloadResolver', () => {
  let resolver: DashboardPreloadResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(DashboardPreloadResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
