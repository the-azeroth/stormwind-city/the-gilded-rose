import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRouting } from './dashboard.routing';
import { DashboardComponent } from './dashboard.component';
import { WidgetModule } from './widget/widget.module';
import { DashboardCalendarModule } from './common/component/dashboard-calendar/dashboard-calendar.module';

import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [DashboardComponent],
  imports: [ButtonModule, CommonModule, DashboardCalendarModule, DashboardRouting, WidgetModule]
})
export class DashboardModule {}
