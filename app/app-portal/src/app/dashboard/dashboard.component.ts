import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WidgetInstance } from './common/model/widget-instance.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  public widgetInstances: WidgetInstance[] = [];

  private readonly subscription: Subscription = new Subscription();

  constructor(private readonly route: ActivatedRoute) {}

  ngOnInit(): void {
    const getWidgetInstances$ = this.route.data.subscribe(data => {
      this.widgetInstances = data['widgetInstances'];
    });
    this.subscription.add(getWidgetInstances$);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onWidgetRemove(widgetId: number): void {
    this.widgetInstances.forEach((w: WidgetInstance, index: number) => {
      if (w.id === widgetId) {
        this.widgetInstances.splice(index, 1);
      }
    });
  }
}
