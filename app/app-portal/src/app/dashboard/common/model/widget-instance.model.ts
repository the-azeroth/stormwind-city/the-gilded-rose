export interface WidgetInstance {
  id?: number;
  title?: string;
  instance?: string;
}
