import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-dashboard-calendar',
  templateUrl: './dashboard-calendar.component.html',
  styleUrls: ['./dashboard-calendar.component.scss']
})
export class DashboardCalendarComponent implements OnInit {
  @Output() onSelect: EventEmitter<string> = new EventEmitter<string>();

  @Output() onClear: EventEmitter<any> = new EventEmitter<any>();

  value: Date;

  numberOfMonths = 2;

  rangeSeparator = 'to';

  selectionMode = 'range';

  showIcon = true;

  dateFormat = 'yy-mm-dd';

  constructor() {}

  ngOnInit(): void {}
}
