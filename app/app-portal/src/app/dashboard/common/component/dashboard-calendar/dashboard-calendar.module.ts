import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DashboardCalendarComponent } from './dashboard-calendar.component';

import { CalendarModule } from 'primeng/calendar';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [DashboardCalendarComponent],
  imports: [ButtonModule, CalendarModule, CommonModule, FormsModule],
  exports: [DashboardCalendarComponent]
})
export class DashboardCalendarModule {}
