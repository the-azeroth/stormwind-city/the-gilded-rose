import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { delay, Observable, of } from 'rxjs';
import { WidgetInstance } from './common/model/widget-instance.model';

@Injectable({
  providedIn: 'root'
})
export class DashboardPreloadResolver implements Resolve<WidgetInstance[]> {
  private readonly widgetInstances: WidgetInstance[] = [
    { id: 1, title: 'Widget 1', instance: 'search' },
    { id: 2, title: 'Widget 2', instance: 'task' },
    { id: 3, title: 'Widget 3', instance: 'search' },
    { id: 4, title: 'Widget 4', instance: 'search' },
    { id: 5, title: 'Widget 5', instance: 'task' },
    { id: 6, title: 'Widget 6', instance: 'bar' },
    { id: 7, title: 'Widget 7', instance: 'bar' },
    { id: 8, title: 'Widget 8', instance: 'search' },
    { id: 9, title: 'Widget 9', instance: 'task' }
  ];

  resolve(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Observable<WidgetInstance[]> {
    return of(this.widgetInstances).pipe(delay(300));
  }
}
