import { Component, OnInit } from '@angular/core';
import { Widget } from '../widget';

@Component({
  selector: 'app-search-widget',
  templateUrl: './search-widget.component.html',
  styleUrls: ['./search-widget.component.scss']
})
export class SearchWidgetComponent implements Widget, OnInit {
  constructor() {}

  ngOnInit(): void {}
}
