import { Component, OnInit } from '@angular/core';
import { Widget } from '../widget';

@Component({
  selector: 'app-task-widget',
  templateUrl: './task-widget.component.html',
  styleUrls: ['./task-widget.component.scss']
})
export class TaskWidgetComponent implements Widget, OnInit {
  constructor() {}

  ngOnInit(): void {}
}
