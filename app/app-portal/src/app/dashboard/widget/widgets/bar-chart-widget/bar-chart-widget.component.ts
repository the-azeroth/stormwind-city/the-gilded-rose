import { Component, OnInit } from '@angular/core';
import { Widget } from '../widget';

@Component({
  selector: 'app-bar-chart-widget',
  templateUrl: './bar-chart-widget.component.html',
  styleUrls: ['./bar-chart-widget.component.scss']
})
export class BarChartWidgetComponent implements Widget, OnInit {
  constructor() {}

  ngOnInit(): void {}
}
