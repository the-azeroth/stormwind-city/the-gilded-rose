import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appWidgetWrapperHost]'
})
export class WidgetWrapperHostDirective {
  constructor(private readonly _viewContainerRef: ViewContainerRef) {}

  get viewContainerRef(): ViewContainerRef {
    return this._viewContainerRef;
  }
}
