import { Component, EventEmitter, Input, OnInit, Output, Type, ViewChild } from '@angular/core';
import { SearchWidgetComponent } from '../widgets/search-widget/search-widget.component';
import { WidgetWrapperHostDirective } from './widget-wrapper-host.directive';
import { Widget } from '../widgets/widget';
import { BarChartWidgetComponent } from '../widgets/bar-chart-widget/bar-chart-widget.component';
import { TaskWidgetComponent } from '../widgets/task-widget/task-widget.component';

@Component({
  selector: 'app-widget-wrapper',
  templateUrl: './widget-wrapper.component.html',
  styleUrls: ['./widget-wrapper.component.scss']
})
export class WidgetWrapperComponent implements OnInit {
  @Input() id: number;
  @Input() title: string;
  @Input() instance: string;
  @Output() onRemove: EventEmitter<number> = new EventEmitter<number>();

  @ViewChild(WidgetWrapperHostDirective, { static: true }) wrapperHost: WidgetWrapperHostDirective;

  constructor() {}

  ngOnInit(): void {
    const viewContainerRef = this.wrapperHost.viewContainerRef;
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent<Widget>(this.getComponent(this.instance));
  }

  getComponent(instance: string): Type<Widget> {
    switch (instance) {
      case 'search':
        return SearchWidgetComponent;
      case 'bar':
        return BarChartWidgetComponent;
      case 'task':
        return TaskWidgetComponent;
    }
  }

  onRemoveWidget(): void {
    this.onRemove.emit(this.id);
  }

  onExpandCollapseWidget(): void {}
}
