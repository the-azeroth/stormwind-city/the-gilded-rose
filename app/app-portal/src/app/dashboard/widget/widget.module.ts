import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetWrapperComponent } from './widget-wrapper/widget-wrapper.component';
import { WidgetWrapperHostDirective } from './widget-wrapper/widget-wrapper-host.directive';
import { SearchWidgetComponent } from './widgets/search-widget/search-widget.component';
import { TaskWidgetComponent } from './widgets/task-widget/task-widget.component';
import { BarChartWidgetComponent } from './widgets/bar-chart-widget/bar-chart-widget.component';

import { PanelModule } from 'primeng/panel';

@NgModule({
  declarations: [
    BarChartWidgetComponent,
    SearchWidgetComponent,
    TaskWidgetComponent,
    WidgetWrapperComponent,
    WidgetWrapperHostDirective
  ],
  imports: [CommonModule, PanelModule],
  exports: [SearchWidgetComponent, TaskWidgetComponent, WidgetWrapperComponent, WidgetWrapperHostDirective]
})
export class WidgetModule {}
