import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { DashboardPreloadResolver } from './dashboard-preload.resolver';

const routes: Routes = [
  {
    path: '',
    resolve: {
      widgetInstances: DashboardPreloadResolver
    },
    component: DashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRouting {}
