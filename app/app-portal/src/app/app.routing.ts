import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

// import { loadRemoteModule } from '@angular-architects/module-federation';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'ng-di',
        loadChildren: () => import('./ng-di/ng-di.module').then(m => m.NgDiModule)
      }
      // {
      //   path: 'workflow',
      //   loadChildren: () =>
      //     loadRemoteModule({
      //       type: 'module',
      //       remoteEntry: 'http://localhost:3001/remoteEntry.js',
      //       exposedModule: './Module'
      //     }).then(m => m.WorkflowModule)
      // }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      relativeLinkResolution: 'legacy',
      scrollPositionRestoration: 'top',
      onSameUrlNavigation: 'reload'
      // preloadingStrategy: 'PreloadAllModules'
    })
  ],
  exports: [RouterModule]
})
export class AppRouting {}
