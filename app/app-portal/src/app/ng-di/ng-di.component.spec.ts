import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgDiComponent } from './ng-di.component';

describe('NgDiComponent', () => {
  let component: NgDiComponent;
  let fixture: ComponentFixture<NgDiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgDiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgDiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
