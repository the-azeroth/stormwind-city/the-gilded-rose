import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompCRoutingModule } from './comp-c-routing.module';
import { CompCComponent } from './comp-c.component';

@NgModule({
  declarations: [CompCComponent],
  imports: [CommonModule, CompCRoutingModule]
})
export class CompCModule {}
