import { Component, OnInit } from '@angular/core';
import { ServiceAService } from '../common/service-a.service';

@Component({
  selector: 'app-comp-c',
  template: `
    <div>Component C</div>
    <a routerLink="/ng-di/comp-b">Component B</a>
  `
})
export class CompCComponent implements OnInit {
  constructor(private readonly serviceA: ServiceAService) {}

  ngOnInit(): void {
    console.log(this.serviceA.instance);
    this.serviceA.instance = 'CompCComponent';
  }
}
