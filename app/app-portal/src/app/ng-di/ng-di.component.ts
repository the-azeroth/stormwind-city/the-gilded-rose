import { Component, OnInit } from '@angular/core';
import { ServiceAService } from './common/service-a.service';

@Component({
  selector: 'app-ng-di',
  template: '<router-outlet></router-outlet>'
})
export class NgDiComponent implements OnInit {
  constructor(private readonly serviceA: ServiceAService) {}

  ngOnInit(): void {
    this.serviceA.instance = 'NgDiComponent';
  }
}
