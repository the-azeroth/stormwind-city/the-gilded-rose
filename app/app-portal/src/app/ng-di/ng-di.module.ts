import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgDiRoutingModule } from './ng-di-routing.module';
import { NgDiComponent } from './ng-di.component';
import { DefaultModule } from './common/default.module';

@NgModule({
  declarations: [NgDiComponent],
  imports: [CommonModule, DefaultModule, NgDiRoutingModule]
})
export class NgDiModule {}
