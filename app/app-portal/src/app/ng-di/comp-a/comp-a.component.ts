import { Component, OnInit } from '@angular/core';
import { ServiceAService } from '../common/service-a.service';

@Component({
  selector: 'app-comp-a',
  template: `
    <div>Component A</div>
    <a routerLink="/ng-di/comp-b">Component B</a>
  `
})
export class CompAComponent implements OnInit {
  constructor(private readonly serviceA: ServiceAService) {}

  ngOnInit(): void {
    console.log(this.serviceA.instance);
    this.serviceA.instance = 'CompAComponent';
    console.log(this.serviceA.instance);
  }
}
