import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompARoutingModule } from './comp-a-routing.module';
import { CompAComponent } from './comp-a.component';
import { DefaultModule } from '../common/default.module';

@NgModule({
  declarations: [CompAComponent],
  imports: [CommonModule, CompARoutingModule, DefaultModule]
})
export class CompAModule {}
