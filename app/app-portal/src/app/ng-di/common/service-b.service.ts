import { Injectable } from '@angular/core';

@Injectable()
export class ServiceBService {
  private _instance: string;

  constructor() {}

  set instance(instance: string) {
    this._instance = 'B' + instance;
  }

  get instance(): string {
    return this._instance;
  }
}
