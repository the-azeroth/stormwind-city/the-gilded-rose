import { Injectable } from '@angular/core';

@Injectable()
export class ServiceAService {
  private _instance: string;

  constructor() {}

  set instance(instance: string) {
    this._instance = 'A' + instance;
  }

  get instance(): string {
    return this._instance;
  }
}
