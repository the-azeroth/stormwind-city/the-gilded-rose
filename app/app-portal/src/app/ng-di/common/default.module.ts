import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceAService } from './service-a.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [ServiceAService]
})
export class DefaultModule {}
