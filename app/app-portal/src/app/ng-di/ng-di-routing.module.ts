import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgDiComponent } from './ng-di.component';

const routes: Routes = [
  {
    path: '',
    component: NgDiComponent,
    children: [
      { path: 'comp-a', loadChildren: () => import('./comp-a/comp-a.module').then(m => m.CompAModule) },
      { path: 'comp-b', loadChildren: () => import('./comp-b/comp-b.module').then(m => m.CompBModule) }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NgDiRoutingModule {}
