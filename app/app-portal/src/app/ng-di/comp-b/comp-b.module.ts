import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompBRoutingModule } from './comp-b-routing.module';
import { CompBComponent } from './comp-b.component';
import { DefaultModule } from '../common/default.module';

@NgModule({
  declarations: [CompBComponent],
  imports: [CommonModule, CompBRoutingModule, DefaultModule]
})
export class CompBModule {}
