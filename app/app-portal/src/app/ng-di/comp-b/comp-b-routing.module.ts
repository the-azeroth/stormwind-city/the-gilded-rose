import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompBComponent } from './comp-b.component';

const routes: Routes = [
  {
    path: '',
    component: CompBComponent,
    children: [{ path: 'comp-c', loadChildren: () => import('../comp-c/comp-c.module').then(m => m.CompCModule) }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompBRoutingModule {}
