import { Component, OnInit } from '@angular/core';
import { ServiceAService } from '../common/service-a.service';

@Component({
  selector: 'app-comp-b',
  template: `
    <div>Component B</div>
    <a routerLink="/ng-di/comp-b/comp-c">Component C</a>
    <router-outlet></router-outlet>
  `
})
export class CompBComponent implements OnInit {
  constructor(private readonly serviceA: ServiceAService) {}

  ngOnInit(): void {
    console.log(this.serviceA.instance);
    this.serviceA.instance = 'CompBComponent';
    console.log(this.serviceA.instance);
  }
}
